package proyecto_3;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;

public class testQueue {

	public Queue<Integer> queue;
	
	@Before
	public void setUp()
	{
		queue = new Queue<Integer>();
		for(int i = 1; i <= 100; i++)
			queue.enqueue(i);
	}
	@Test
	public void test() 
	{
		for(int i = 1; i < 100; i++)
			assertTrue(i == queue.dequeue());
	}

}
