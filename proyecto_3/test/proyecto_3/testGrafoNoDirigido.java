package proyecto_3;

import java.io.IOException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.GrafoNoDirigido;
import model.data_structures.Kruskal;
import model.logic.MVCModelo;

public class testGrafoNoDirigido 
{
	GrafoNoDirigido<Integer, ObjetoTemporal> grafo;
	GrafoNoDirigido<Integer, String> grafo2;

	MVCModelo modelo;
	@Before
	public void setUp() throws IOException
	{
		modelo = new MVCModelo();
		grafo = new GrafoNoDirigido<Integer, ObjetoTemporal>(9);

		grafo.addVertex(1, new ObjetoTemporal(1,"uno"));
		grafo.addVertex(2, new ObjetoTemporal(2,"dos"));
		grafo.addVertex(3, new ObjetoTemporal(3,"tres"));
		grafo.addVertex(4, new ObjetoTemporal(4,"cuatro"));
		grafo.addVertex(5, new ObjetoTemporal(5,"cinco"));
		grafo.addVertex(6, new ObjetoTemporal(6,"seis"));
		grafo.addVertex(7, new ObjetoTemporal(7,"siete"));
		grafo.addVertex(8, new ObjetoTemporal(8,"ocho"));
		
		grafo.addEdge(1, 2, 3);
		grafo.addEdge(1, 5, 7);
		grafo.addEdge(2, 3, 4);
		grafo.addEdge(2, 4, 8);
		grafo.addEdge(5, 2, 9);

		grafo.addEdge(8, 7, 35);

		//modelo.cargar();
		

	}


	@Test
	public void testGrafo()
	{
		grafo.uncheck();
		grafo.bfs(1);
		int maxCamino = 0, idMax = 0;
		for(int i = 0; i < grafo.distTo.length;i++)
		{
			if(grafo.distTo[i] > maxCamino)
			{
				maxCamino = grafo.distTo[i];
				idMax = i;
			}
		}
		System.out.println(grafo.getVertice(idMax).papa.identificacion);
	}

	//	public void setUp2()
	//	{
	//		grafo2 = new GrafoNoDirigido<Integer, String>(5);
	//		grafo2.addVertex(1, "uno");
	//		grafo2.addVertex(2, "dos");
	//		grafo2.addVertex(3, "tres");
	//		grafo2.addVertex(4, "cuatro");
	//		grafo2.addVertex(5, "cinco");
	//
	//		grafo2.addEdge(1, 2, 3);
	//		grafo2.addEdge(1, 5, 7);
	//		grafo2.addEdge(2, 3, 4);
	//		grafo2.addEdge(2, 4, 8);
	//		grafo2.addEdge(5, 2, 9);
	//	}
	//
	//	@Test
	//	public void testDijkstra()
	//	{
	//		setUp2();
	//		Dijkstra<ObjetoTemporal> dij = new Dijkstra<ObjetoTemporal>(grafo, 1);
	//		if(dij.hasPathTo(4))
	//		{
	//			Iterator<Arco<Integer>> iterator = dij.pathTo(4).iterator();
	//			Arco<Integer> iterador = iterator.next();
	//			while(iterador != null)
	//			{
	//				System.out.println("Desde: " + iterador.iniVertice + " hasta: " + iterador.finVertice);
	//				iterador = iterator.next(); 
	//			}
	//		}
	//	}

}
