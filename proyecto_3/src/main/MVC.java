package main;

import java.io.IOException;

import controller.Controller;

public class MVC 
{
	//-----------------------------------
	// Atributos
	//-----------------------------------
	/**
	 * Conexión con el controller
	 */
	private static Controller controller;
	//-----------------------------------
	// Constructor
	//-----------------------------------
	/**
	 * Método principal del programa
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException 
	{
		controller = new Controller();
		controller.run();
	}

}
