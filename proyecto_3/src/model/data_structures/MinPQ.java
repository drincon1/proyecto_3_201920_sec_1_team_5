package model.data_structures;

public class MinPQ<K extends Comparable<K>> 
{
	/**
	 * Elementos de la cola
	 * pq[1..N]
	 */
	private K[] pq;

	/**
	 * Número de elementos
	 */
	private int N = 0; 

	public MinPQ(int maxN)
	{
		pq = (K[]) new Comparable[maxN+1];	
	}

	public MinPQ() {
		this(1);
	}

	public boolean isEmpty()
	{
		return N == 0;
	}

	public int size()
	{
		return N;
	}

	public void insert(K v)
	{
		if (N == pq.length - 1) resize(2 * pq.length);
		
		pq[++N] = v;
		swim(N);
	}

	public K delMin()
	{
		K min = pq[1];
		exch(1, N--);
		pq[N+1] = null;
		sink(1);
		return min;
	}

	private boolean less(int i, int j)
	{  
		return pq[i].compareTo(pq[j]) > 0;  
	}

	private void exch(int i, int j)
	{ 
		K t = pq[i]; 
		pq[i] = pq[j]; 
		pq[j] = t;  
	}

	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	private void sink(int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1)) j++;
			if (!less(k, j)) 
				break;
			exch(k, j);
			k = j;
		}
	}

	private void resize(int capacity) 
	{
		assert capacity > N;
		K[] temp = (K[]) new Comparable[capacity];
		for (int i = 1; i <= N; i++) {
			temp[i] = pq[i];
		}
		pq = temp;
	}

}
