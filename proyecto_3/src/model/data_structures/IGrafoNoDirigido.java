package model.data_structures;

//K == id de la zona
//V == longitud, latiud, nombre y ide de la zona
public interface IGrafoNoDirigido<K,V> 
{
	/**
	 * Número de vértices;
	 * @return El número de vértices (int)
	 */
	int V();
	
	/**
	 * Número de arcos. 
	 * Cada arco No dirigido debe contarse una única vez.
	 * @return El número de arcos (int)
	 */
	int E();

	/**
	 * Adiciona el arco No dirigido entre el vértice IdVertexIni y el vértice IdVertexFin. 
	 * El arco tiene el costo cost.
	 * @param idVertexIni El vertice inicial. idVertexIni != null
	 * @param idVertexFin El vertice destino. idVertexFin != null
	 * @param cost El costo del arco. cost >= 0;
	 */
	void addEdge(K idVertexIni, K idVertexFin, double cost);
	
	/**
	 * Obtener la información de un vértice. 
	 * Si el vértice no existe retorna null.
	 * @param idVertex La identificación del vertice. idVertex != null
	 * @return La información del vertice. Puede retonar null
	 */
	V getInfoVertex(K idVertex);
	
	/**
	 * Modificar la información del vértice idVertex
	 * @param idVertex La identifiación del vertice al que se le va a cambiar la información. idVertex != null
	 * @param infoVertex La nueva información que se va a agregar. inforVertex != null
	 */
	void setInfoVertex(K idVertex, V infoVertex);
	
	/**
	 * Obtener el costo de un arco. 
	 * Si el arco no existe, retorna -1
	 * @param idVertexIni El vertice inicial. idVertexIni != null
	 * @param idVertexFin El vertice destino. idVertexFin != null
	 * @return El costo del arco. -1 si no existe el arco
	 */
	double getCostArc(K idVertexIni, K idVertexFin);
	
	
	/**
	 * Modificar el costo del arco entre los vértices idVertexIni e idVertexFin
	 * @param idVertexIni El vertice inicial. idVertexIni != null
	 * @param idVertexFin El vertice destino. idVertexFin != null
	 * @param cost El nuevo costo del arco. cost >= 0
	 */
	void setCostArc(K idVertexIni, K idVertexFin, double cost);
	
	/**
	 * Adiciona un vértice con un Id único. 
	 * El vértice tiene la información InfoVertex.
	 * @param idVertex La identificación del nuevo vertice. idVertex != null
	 * @param infoVertex La informacion del nuevo vertice. infoVertex != null
	 */
	void addVertex(K idVertex, V infoVertex);
	
	/**
	 * Retorna los identificadores de los vértices adyacentes a idVertex
	 * @param idVertex La identificación del vertice al que se le va a buscar las conexiones. idVertex != null
	 * @return Un iterador que contiene los identificadores de las llaves conectadas a idVertex
	 */
	Iterable<K> adj (K idVertex);
	
	/**
	 * Desmarca todos los vértices del grafo
	 */
	void uncheck();
	
	/**
	 * Ejecuta la búsqueda de profundidad sobre el grafo con el vértice s como origen. 
	 * Los vértices resultado de la búsqueda quedan marcados y deben tener información
	 * que pertenecen a una misma componente conectada.
	 * @param s La identifiación del vertice origen. s != null
	 */
	void dfs(K s);
	
	/**
	 * Obtiene la cantidad de componentes conectados del grafo. 
	 * Cada vértice debe quedar marcado y debe reconocer a cuál componente conectada pertenece. 
	 * En caso de que el grafo esté vacío, retorna 0.
	 * @return Número de componentes conectados del grafo.
	 */
	int cc();
	
	/**
	 * Obtiene los vértices alcanzados a partir del vértice idVertex 
	 * después de la ejecución de los metodos dfs(K) y cc().
	 * @param idVertex Identificación del vertice que comienza dfs. idVertex != null
	 * @return Un iterable que contiene las identificaciones de vertices conectados a idVertex
	 */
	Iterable<K> getCC(K idVertex);
	
}
