package model.data_structures;

public class Arco<K> implements Comparable<Arco<K>>
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	/**
	 * El vertice inicial del arco
	 */
	public K iniVertice;
	
	/**
	 * El vertice final del arco
	 */
	public K finVertice;
	
	/**
	 * El costo 1 del arco == harversine
	 */
	public double costo;
	
	/**
	 * El costo 2 del arco == tiempo de viaje
	 */
	public double tiempo_de_viaje;
	
	/**
	 * El costo 3 del arco == velocidad del viaje
	 */
	public double velocidad;
	
	
	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	
	public Arco(K iniVertice, K finVertice, double costo)
	{
		this.iniVertice = iniVertice;
		this.finVertice = finVertice;
		this.costo = costo;
	}
	
	public void calcularVelocidad()
	{
		if(tiempo_de_viaje != 0)
			velocidad = costo / tiempo_de_viaje;
		else
			velocidad = 0;
	}
	
	public K either()
	{
		return iniVertice;
	}
	
	public K other(K vertex)
	{
		if(vertex == iniVertice) return finVertice;
		else return iniVertice;
	}

	@Override
	public int compareTo(Arco<K> o) 
	{
		if(this.costo < o.costo)
			return -1;
		else if(this.costo > o.costo)
			return 1;
		return 0;
	}
	
}
