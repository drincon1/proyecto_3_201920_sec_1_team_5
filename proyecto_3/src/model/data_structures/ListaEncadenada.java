package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<K> implements Iterable<K>
{
	public Nodo<K> primero;


	/**
	 * @param primero
	 */
	public ListaEncadenada() 
	{

	}

	public void add(K data)
	{
		if(primero == null)
			primero = new Nodo<K>(data);
		else
		{
			Nodo<K> tempo = primero;
			while(tempo != null)
			{			
				if(tempo.sig == null)
				{
					tempo.sig = new Nodo<K>(data);
					break;
				}
				tempo = tempo.sig;
			}
		}

	}


	public Nodo<K> buscar(K data)
	{
		Nodo<K> tempo = primero;
		while(tempo != null)
		{			
			if(tempo.value.equals(data))
			{
				return tempo;
			}
			tempo = tempo.sig;
		}
		
		return null;

	}

	@Override
	public Iterator<K> iterator() 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
