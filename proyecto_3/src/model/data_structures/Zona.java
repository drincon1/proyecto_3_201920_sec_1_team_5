package model.data_structures;

import model.logic.Interseccion;

public class Zona 
{
	public ListaEncadenada<Interseccion> intersecciones;
	
	public int movement_id;
	
	public double latitud;
	
	public double longitud;
	
	
	public Zona(int movement_id, double latitud, double longitud)
	{
		this.movement_id = movement_id;
		this.latitud = latitud;
		this.longitud = longitud;
		intersecciones = new ListaEncadenada<Interseccion>();
	}
	
	
}
