package model.data_structures;

/**
 * Algorithms pg. 151
 * @author daniel
 *
 * @param <K>
 */
public class Queue<K extends Comparable<K>>
{
	private Nodo<K> first;

	private Nodo<K> last;

	private int N;

	public boolean isEmpty() {  return first == null;  }  
	public int size()        {  return N;  }

	public void enqueue(K item)
	{  
		Nodo<K> oldlast = last;
		last = new Nodo<K>(item);
		last.sig = null;
		if (isEmpty()) first = last;
		else           oldlast.sig = last;
		N++;
	}

	public K dequeue()
	{  // Remove item from the beginning of the list.
		K item = null;
		if(!isEmpty())
		{
			item = first.value;
			first = first.sig;
			if (isEmpty()) last = null;
			N--;
		}
		return item;
	}
}
