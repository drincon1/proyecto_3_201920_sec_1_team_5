package model.data_structures;

import java.util.Iterator;


/**
 * Códigos varios sacados de: https://algs4.cs.princeton.edu/home/
 * @author daniel
 *
 * @param <K>
 * @param <V>
 */

public class GrafoNoDirigido<K extends Comparable<K>,V>  implements IGrafoNoDirigido<K,V>
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	/**
	 * Número de vertices
	 */
	public int V = 0;

	/**
	 * Número de arcos
	 */

	public int E;


	/**
	 * Un arreglo de listas encadenadas
	 * En cada posición estan los arcos del vertice
	 */
	public ListaEncadenada<Arco<K>>[] adj;

	/**
	 * Tabla Hash Linear Proving con los vertices
	 */
	public HashTable<K,Vertice<K,V>> vertices;

	/**
	 * Vertices marcados
	 */
	public boolean[] marked;

	/**
	 * Número de nodos contados
	 */
	public int count;

	/**
	 * 
	 */
	public int[] id;

	/**
	 * En cada posición se guarda la distancia de un vertice s hasta el vertice distTo[i]
	 */
	public int[] distTo; 

	/**
	 * En cada posición está el id del vertice del cual se llego.
	 */
	public int[] edgeTo;

	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	/**
	 * Constructor de un grafo no dirigido
	 * El grafo se crea sin arcos
	 * @param V Número de vértices
	 */
	public GrafoNoDirigido(int V) 
	{
		//Número de vertices
		this.V = V;

		//Agrega una lista encadenada a cada posicion del arreglo adj
		adj = new ListaEncadenada[V+1];
		for(int v = 0; v < adj.length; v++)
			adj[v] = new ListaEncadenada<Arco<K>>();

		//Tabla de hash de los vertices
		vertices = new HashTable<K, Vertice<K,V>>(V);

		//El arreglo marked se crea con V posiciones porque puede que todos los vertices queden marcados
		marked = new boolean[V+1];

		//Cuando se crea el grafo no hay vertices marcados
		count = 0;

		id = new int[V+1];		

		distTo = new int[V+1];

		edgeTo = new int[V+1];
	}

	//---------------------------------------------
	// Funcionamiento 
	//---------------------------------------------

	@Override
	public int V() 
	{
		return V;
	}

	@Override
	public int E() 
	{
		return E;
	}

	@Override
	public void addEdge(K idVertexIni, K idVertexFin, double cost) 
	{			
		int ini = (Integer) idVertexIni;
		int fin = (Integer) idVertexFin;

		adj[ini].add(new Arco<K>(idVertexIni, idVertexFin, cost));
		adj[fin].add(new Arco<K>(idVertexFin, idVertexIni, cost));

		E++;
	}

	@Override
	public V getInfoVertex(K idVertex) 
	{
		if(vertices.get(idVertex) != null)
			return vertices.get(idVertex).valor;
		return null;
	}

	@Override
	public void setInfoVertex(K idVertex, V infoVertex) 
	{
		vertices.put(idVertex, new Vertice<K,V>(idVertex, infoVertex));
	}

	@Override
	public double getCostArc(K idVertexIni, K idVertexFin) 
	{
		Nodo<Arco<K>> tempo = adj[(Integer) idVertexIni].primero;
		while(tempo != null)
		{
			if(tempo.value.finVertice.equals(idVertexFin))
				return tempo.value.costo;
			tempo = tempo.sig;
		}

		return -1;
	}

	public Arco<K> getArco(K idVertexIni, K idVertexFin)
	{
		Nodo<Arco<K>> tempo = adj[(Integer) idVertexIni].primero;
		while(tempo != null)
		{
			if(tempo.value.finVertice.equals(idVertexFin))
				return tempo.value;
			tempo = tempo.sig;
		}
		return null;
	}

	public Vertice<K,V> getVertice(K id)
	{
		return vertices.get(id);
	}

	@Override
	public void setCostArc(K idVertexIni, K idVertexFin, double cost) 
	{
		Nodo<Arco<K>> tempo = adj[(Integer) idVertexIni].primero;
		while(tempo != null)
		{
			if(tempo.value.finVertice.equals(idVertexFin))
			{
				tempo.value.costo = cost;
				break;
			}
			tempo = tempo.sig;
		}
	}

	@Override
	public void addVertex(K idVertex, V infoVertex) 
	{
		Vertice<K,V> vertice = new Vertice<K,V>(idVertex, infoVertex);
		vertices.put(idVertex, vertice);
	}

	@Override
	public Iterable<K> adj(K idVertex) 
	{
		return null;
	}

	@Override
	public void uncheck() 
	{
		marked = new boolean[V+1];
		id = new int[V+1];
		Iterator<K> iterator = vertices.iterator(); 
		K i = null;

		while(iterator.hasNext())
		{
			i = iterator.next();
			vertices.get(i).marcado = false;
			vertices.get(i).papa = null;
			vertices.get(i).distanceTo = 0.0;
		}
		count = 0;

	}

	@Override
	public void dfs(K s)
	{
		marked[(Integer) s] = true;
		vertices.get(s).marcado = true;

		id[(Integer) s] = count;
		vertices.get(s).cc = count;

		Nodo<Arco<K>> tempo = adj[(Integer) s].primero;
		while(tempo != null)
		{
//			if(!marked[(Integer) tempo.value.finVertice])
			if(!vertices.get(tempo.value.finVertice).marcado)
				dfs(tempo.value.finVertice);
			tempo = tempo.sig;
		}

	}

	@Override
	public int cc() 
	{
		for(int s = 0; s < V();s++)
		{
			K tempo = vertices.getKeys()[s];
			if(tempo != null)
			{
//				if(!marked[(Integer) tempo ])
				if(!vertices.get(tempo).marcado)
				{
					dfs(tempo);
					count++;
				}
			}
		}
		return count;
	}

	@Override
	public Iterable<K> getCC(K idVertex) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Método que da cuantos vertices hay en cada componente
	 */
	public int[] componentesGrandes()
	{
		int[] arreglo = new int[count];
		Iterator<K> iterator = vertices.iterator();
		K iterador = iterator.next();
		while(iterator.hasNext())
		{
			arreglo[vertices.get(iterador).cc] += 1;
			iterador = iterator.next();
		}
		return arreglo;
	}


	public void bfs(K s)
	{
		uncheck();
		Queue<K> q = new Queue<K>();
		q.enqueue(s);
		marked[(int)s] = true;
		distTo[(int)s] = 0;

		while (!q.isEmpty()) 
		{
			K v = q.dequeue();
			Nodo<Arco<K>> tempo = adj[(Integer) v].primero;
			while(tempo != null)
			{
				K w = tempo.value.finVertice;
				if(!marked[(int) w])
				{
					q.enqueue(w);
					marked[(int)w] = true;
					vertices.get(w).papa = vertices.get(v);
					edgeTo[(int)w] = (int) v;
					distTo[(int)w] = distTo[(int)v] + 1;
				}
				tempo = tempo.sig;
			}
		}
	}

	public Queue<Vertice<K,V>> colaMax = new Queue<Vertice<K,V>>();


	public void maxCosto(K s,double costo)
	{
		//Vertice que llega como parametro
		Vertice<K, V> vertice = getVertice(s);
		//Marca el vertice
		vertice.marcar();

		//Añade el vertice a la cola
		colaMax.enqueue(vertice);

		//El primer arco del vertice
		Nodo<Arco<K>> tempo = adj[(int) s].primero;

		//El vertice destino del arco
		Vertice<K,V> fin = null;
		//Recorre los arcos del vertice 'vertice'
		while(tempo != null)
		{
			//inicializa 'fin' con el vertice destino del arco
			fin = vertices.get(tempo.value.finVertice);
			//esta condición está para no cambiar el costoAcumulado
			if(!fin.marcado)
			{
				//El costo acumulado del vertice destino
				fin.costoAcumulado = tempo.value.costo + vertice.costoAcumulado;
				//Si el costo acumulado es menor al costo deseado se va a agregar y marcar
				if(fin.costoAcumulado <= costo)
				{
					maxCosto(fin.identificacion, costo);
				}
			}
			//siguiente arco de 'vertice'
			tempo = tempo.sig;
		}
	}

	//---------------------------------------------
	// Prim - Algorithms
	//---------------------------------------------
	private MinPQ<Arco<K>> pq;
	public Stack<Arco<K>> stack;

	public Queue<Arco<K>> Prim(K s)
	{
		uncheck();
		Queue<Arco<K>> mst = new Queue<Arco<K>>();
		pq = new MinPQ<Arco<K>>();
		stack = new Stack<Arco<K>>();
		Vertice<K,V> v1,v2;
		visit(vertices.get(s).identificacion);

		while (!pq.isEmpty() && mst.size() < V() - 1)
		{
			Arco<K> e = pq.delMin();
			K v = e.either(), w = e.other(v);
			v1 = getVertice(v);
			v2 = getVertice(w);

			if(v1.marcado && v2.marcado)
				continue;
			v2.papa = v1;
			mst.enqueue(e);
			stack.push(e);
			if(!v1.marcado)
				visit(v);
			if(!v2.marcado)
				visit(w);
		}

		return mst;
	}

	private void visit(K s)
	{
		getVertice(s).marcar();
		Nodo<Arco<K>> temp = adj[(int) s].primero;
		while(temp != null)
		{
			K v = temp.value.other(s);
			if(!getVertice(v).marcado)
				pq.insert(temp.value);
			temp = temp.sig;
		}
	}












}
