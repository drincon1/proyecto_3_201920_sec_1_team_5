package model.data_structures;
/**
 * Código sacado de: https://algs4.cs.princeton.edu/44sp/DijkstraSP.java.html
 * @author daniel
 *
 * @param <V>
 */

public class Dijkstra<V>
{
    private double[] distTo;          
    private Arco<Integer>[] edgeTo;    
    private IndexMinPQ<Double> pq;    


    public Dijkstra(GrafoNoDirigido<Integer,V> G, int s) {


        distTo = new double[G.V()+1];
        edgeTo = new Arco[G.V()+1];

        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[s] = 0.0;

        
        pq = new IndexMinPQ<Double>(G.V());
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) 
        {
            int v = pq.delMin();
            Nodo<Arco<Integer>> arco = G.adj[v].primero;
            while(arco != null)
            {
            	relax(arco.value);
            	arco = arco.sig;
            }
        }

    }

    private void relax(Arco<Integer> e) {
        int v = e.iniVertice, w = e.finVertice;
        if (distTo[w] > distTo[v] + e.costo) {
            distTo[w] = distTo[v] + e.costo;
            edgeTo[w] = e;
            if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
            else                pq.insert(w, distTo[w]);
        }
    }

    public double distTo(int v) {
        return distTo[v];
    }

    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }


    public Iterable<Arco<Integer>> pathTo(int v) {
        if (!hasPathTo(v)) return null;
        Stack<Arco<Integer>> path = new Stack<Arco<Integer>>();
        for (Arco<Integer> e = edgeTo[v]; e != null; e = edgeTo[e.iniVertice]) {
            path.push(e);
        }
        return path;
    }


   


}

