package model.data_structures;
import model.data_structures.Queue;
/**
 * Código sacado de: https://algs4.cs.princeton.edu/43mst/KruskalMST.java.html
 * @author daniel
 *
 * @param <K>
 * @param <V>
 */
public class Kruskal<K extends Comparable<K>,V>
{

	//-----------------------------------
	// Atributos
	//-----------------------------------
	public double weight;                        
	public Queue<Arco<K>> mst = new Queue<Arco<K>>();

	//-----------------------------------
	// Constructor
	//-----------------------------------
	public Kruskal(GrafoNoDirigido<K,V> G, int cc) 
	{
		MinPQ<Arco<K>> pq = new MinPQ<Arco<K>>();
		for(int i = 0; i < G.adj.length; i++)
		{
			Nodo<Arco<K>> tempo = G.adj[i].primero;
			while(tempo != null)
			{
				pq.insert(tempo.value);
				tempo = tempo.sig;
			}
		}
		UF uf = new UF(G.V()+1);
		while (!pq.isEmpty() && mst.size() < G.V() - 1)
		{
			Arco<K> e = pq.delMin();
			K v = e.either();
			K w = e.other(v);
			if (uf.find((int)v) != uf.find((int)w) && (G.getVertice(v).cc == cc && G.getVertice(w).cc == cc)) 
			{ 
				uf.union((int)v, (int)w);  
				mst.enqueue(e);  
				weight += e.costo;
			}
		}
	}
}
