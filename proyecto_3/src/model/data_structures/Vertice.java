package model.data_structures;

public class Vertice<K,V> implements Comparable<Vertice<K,V>>
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	/**
	 * Si el vertice está marcado
	 */
	public boolean marcado;
	
	/**
	 * Identificación del vertice
	 */
	public K identificacion;
	
	/**
	 * Valor del vertice
	 */
	public V valor;
	
	/**
	 * Componente a la que hace parte
	 */
	public int cc;
	
	/**
	 * El vertice que conecta a este vertice con el resto
	 */
	public Vertice<K,V> papa;
	
	/**
	 * El costo acumulado del vertice hasta cierto punto
	 */
	public double costoAcumulado;
	
	/**
	 * Distancia para llegar a ese vertice
	 */
	public double distanceTo;
	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	
	public Vertice(K identificacion, V valor)
	{
		marcado = false;
		this.identificacion = identificacion;
		this.valor = valor;
	}
	//---------------------------------------------
	// Métodos 
	//---------------------------------------------
	
	public void marcar()
	{
		marcado = true;
	}

	@Override
	public int compareTo(Vertice<K, V> o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
