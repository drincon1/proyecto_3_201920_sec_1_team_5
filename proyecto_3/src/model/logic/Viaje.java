package model.logic;

public class Viaje 
{
	//-----------------------------------
	// Atributos
	//-----------------------------------

	/**
	 * es la zona de origen
	 */
	public int sourceid;

	/**
	 * es la zona destino
	 */
	public int distid;

	/**
	 * el día de la semana
	 */
	public int dia;

	/**
	 * es el tiempo promedio de viaje entre las dos zonas
	 */
	public double mean_travel_time;

	/**
	 * es la desviación estándar de los tiempos de viaje
	 */
	public double standard_deviation_travel_time;
	
	/**
	 * tiempo de viaje medio geométrico
	 */
	public double geometric_mean_travel_time;
	
	/**
	 * tiempo de viaje de desviación estándar geométrica
	 */
	public double geometric_standard_deviation_travel_time;

	//-----------------------------------
	// Constructor
	//-----------------------------------
	/**
	 * Método constructor de la clase Viaje
	 * @param sourceid La zona de origen. sourceid >= 0
	 * @param distid La zona destino. distid >= 0
	 * @param dia El día de la semana. dia >= 1 && dia <= 7
	 * @param mean_travel_time El tiempo promedio de viaje entre las dos zonas. mean_travel_time >= 0
	 * @param standard_deviation_travel_time La desviación estándar de los tiempos de viaje. standard_deviation_travel_time >= 0
	 * @param geometric_mean_travel_time El tiempo de viaje medio geométrico. geometric_mean_travel_time >= 0
	 * @param geometric_standard_deviation_travel_time El tiempo de viaje de desviación estándar geométrica. geometric_standard_deviation_travel_time >= 0
	 */
	public Viaje(int sourceid, int distid, int dia, double mean_travel_time, double standard_deviation_travel_time,
			double geometric_mean_travel_time, double geometric_standard_deviation_travel_time) {
		this.sourceid = sourceid;
		this.distid = distid;
		this.dia = dia;
		this.mean_travel_time = mean_travel_time;
		this.standard_deviation_travel_time = standard_deviation_travel_time;
		this.geometric_mean_travel_time = geometric_mean_travel_time;
		this.geometric_standard_deviation_travel_time = geometric_standard_deviation_travel_time;
	}



}
