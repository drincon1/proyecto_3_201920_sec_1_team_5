package model.logic;

import java.util.Iterator;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Nodo;

public class Maps extends MapView
{

	//---------------------------------------------
	// Atributos
	//---------------------------------------------

	/**
	 * Mapa cargado de JxMaps
	 */
	private Map map;


	Iterator<Integer> llaves;

	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	/**
	 * 
	 * @param grafo
	 */
	public Maps(GrafoNoDirigido<Integer, Interseccion> grafo)
	{
		llaves = grafo.vertices.iterator();

		setOnMapReadyHandler(new MapReadyHandler() 
		{
			public void onMapReady(MapStatus status) 
			{
				if (status == MapStatus.MAP_STATUS_OK) 
				{
					map = getMap();
					initMap(map,grafo);
				}
			}
		});
	}

	//---------------------------------------------
	// Metodos 
	//---------------------------------------------
	public void initMap(Map map,GrafoNoDirigido<Integer, Interseccion> grafo)
	{
		MapOptions options = new MapOptions();
		MapTypeControlOptions controlOptions = new MapTypeControlOptions();
		controlOptions.setPosition(ControlPosition.TOP_RIGHT);

		options.setMapTypeControlOptions(controlOptions);

		map.setOptions(options);
		map.setCenter(new LatLng(4.597714, -74.094723));
		map.setZoom(12.0);

		CircleOptions co = new CircleOptions();

		co.setFillOpacity(0.35);
		co.setStrokeWeight(1);
		co.setStrokeOpacity(0.2);
		co.setStrokeColor("00FF00");

		PolylineOptions polyoptions = new PolylineOptions();
		polyoptions.setGeodesic(false);
		polyoptions.setStrokeColor("#FF0000");
		polyoptions.setStrokeOpacity(1.0);
		polyoptions.setStrokeWeight(2.0);


		int id = -1;

		Interseccion inter = null;
		Interseccion inter_llegada = null;
		while(llaves.hasNext())
		{
			id = llaves.next();

			inter = grafo.getInfoVertex(id);
			Circle circle = new Circle(map);
			circle.setCenter(new LatLng(inter.latitud,inter.longitud));
			circle.setOptions(co);
			circle.setRadius(10);

			Nodo<Arco<Integer>> arco = grafo.adj[inter.id].primero;
			while(arco != null)
			{
				inter_llegada = grafo.getInfoVertex(arco.value.finVertice);
				LatLng[] path = {new LatLng(inter.latitud,inter.longitud),new LatLng(inter_llegada.latitud, inter_llegada.longitud)};
				Polyline polyline = new Polyline(map);
				polyline.setPath(path);
				polyline.setOptions(polyoptions);
				arco = arco.sig;
			}
		}
	}

	public Map mapa()
	{
		return map;
	}




}
