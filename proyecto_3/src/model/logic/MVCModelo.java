package model.logic;

import java.awt.BorderLayout;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.opencsv.CSVReader;

import model.data_structures.Arco;
import model.data_structures.Dijkstra;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.HashTable;
import model.data_structures.Kruskal;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.Vertice;
import model.data_structures.Zona;

public class MVCModelo 
{
	//-----------------------------------
	// Atributos
	//-----------------------------------
	/**
	 * Tabla de Hash con todos los viajes de la semanas del primer trimestre
	 * El parametro Integer se guarda la identificación de salida del viaje
	 */
	public HashTable<String, Viaje> viajes;

	/**
	 * Grafo no dirigido con las intersecciones y conexiones del mapa
	 */
	public GrafoNoDirigido<Integer, Interseccion> mapa;

	/**
	 * Grafo no dirigido nuevo de zonas acumuladas
	 */
	public GrafoNoDirigido<Integer, Zona> zonas;

	//-----------------------------------
	// Constructor
	//-----------------------------------
	/**
	 * Método constructor de la clase MVCModelo
	 */
	public MVCModelo()
	{
		viajes = new HashTable<String, Viaje>();

	}

	//-----------------------------------
	// Carga
	//-----------------------------------

	/**
	 * Método que carga la infomación de las semanas del trimestre 1 y el archivo.json
	 * @throws IOException
	 */
	public void cargar() throws IOException
	{
		CSVReader reader = null;
		FileReader jsonReader = null;
		JSONParser parser = new JSONParser();
		long V = 0;

		try
		{
			//Lector del archivo
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));
			String [] nextLine;
			//Lee la final donde está el encabezado
			nextLine = reader.readNext();	
			//Lee la siguiente linea y comienza el ciclo
			while((nextLine = reader.readNext()) != null)
			{
				//Guarda la información para crear un nuevo viaje
				int sourceid = Integer.parseInt(nextLine[0]);
				int dstid = Integer.parseInt(nextLine[1]);
				int dow = Integer.parseInt(nextLine[2]);
				double mean_travel_time = Double.parseDouble(nextLine[3]);
				double standard_deviation_travel_time = Double.parseDouble(nextLine[4]);
				double geometric_mean_travel_time = Double.parseDouble(nextLine[5]);
				double geometric_standar_deviation_travel_time = Double.parseDouble(nextLine[6]);

				//Crea un nuevo viaje con la información 
				Viaje viaje = new Viaje(sourceid,dstid,dow,mean_travel_time,standard_deviation_travel_time,geometric_mean_travel_time,geometric_standar_deviation_travel_time);

				//Agrega el viaje a la tabla
				viajes.put(sourceid+"-"+dstid+"-"+dow, viaje);

			}

			jsonReader = new FileReader("./data/vertices.json");
			Object obj = parser.parse(jsonReader);
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray info_grafo = (JSONArray)jsonObject.get("info grafo");
			JSONObject numVertices = (JSONObject) info_grafo.get(0);
			V = (long) numVertices.get("#Vertices");			
			mapa = new GrafoNoDirigido<Integer, Interseccion>((int) V);

			JSONArray vertices = (JSONArray) jsonObject.get("vertices");
			for(int i = 0; i < vertices.size(); i++)
			{
				JSONObject vertice = (JSONObject) vertices.get(i);
				long id = (long) vertice.get("id");
				double log = (double) vertice.get("log");
				double lat = (double) vertice.get("lat");
				long MOVEMENT_ID = (long) vertice.get("MOVEMENT_ID");
				mapa.addVertex((int)id, new Interseccion((int)id,log,lat,(int)MOVEMENT_ID));

				JSONArray arcos = (JSONArray) vertice.get("arcos");
				for(int j = 0; j < arcos.size(); j++)
				{
					JSONObject arco = (JSONObject) arcos.get(j);
					long id_arco = (long) arco.get("id");
					double costo = (double) arco.get("costo");
					if(mapa.getCostArc((int) id, (int) id_arco) == -1)
						mapa.addEdge((int) id, (int) id_arco, costo);

				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			reader.close();
		}
	}


	/**
	 * Método que actualiza los costos de los arcos
	 */
	public void cambiarCostos()
	{
		//Identificación de cada vertice del mapa
		Iterator<Integer> iterator = mapa.vertices.iterator();
		int iterador = -1;

		while(iterator.hasNext())
		{
			//guarda el id del primer vertice
			iterador = iterator.next();

			//Intersección que tiene el id de iterador
			Interseccion inter = mapa.vertices.get(iterador).valor;

			//El primer arco de inter
			Nodo<Arco<Integer>> arco = mapa.adj[inter.id].primero;

			//ciclo que recorre los arcos de inter
			while(arco != null)
			{
				//intersección de llegada del arco 'arco'
				Interseccion inter_llegada = mapa.vertices.get(arco.value.finVertice).valor;

				double tiempo = 0;

				//boolean usado para indicar que se encontró un viaje que tenga como 
				//sourceid la zona uber de inter y dstid la zona uber de inter_llegada
				boolean encontrado = false;

				//ciclo que busca un viaje en cada día de la semana
				for(int i = 1; i <= 7  & !encontrado; i++)
				{

					//Entra si encuentra un viaje con las especificaciones
					if(viajes.get(inter.movement_id+"-"+inter_llegada.movement_id+"-"+i) != null)
					{
						tiempo = viajes.get(inter.movement_id+"-"+inter_llegada.movement_id+"-"+i).mean_travel_time;		
						encontrado = true;
					}

				}

				//Si encontró un viaje que tenga la zona origen y zona destino se le asigna el tiempo promedio al arco correspondiente
				if(encontrado)
				{
					mapa.getArco(inter.id, inter_llegada.id).tiempo_de_viaje = tiempo;
				}
				//Si no encontró un viaje que tenga la zona origen o zona destino del arco.
				else
				{
					//Si las zonas uber de los vertices son iguales se le asigna 10 segundos por default
					if(inter.movement_id == inter_llegada.movement_id)
					{
						mapa.getArco(inter.id, inter_llegada.id).tiempo_de_viaje = 10;
					}

					//Si las zonas uber de los vertices son diferentes se le asiga 100 segundos por default
					else
						mapa.getArco(inter.id, inter_llegada.id).tiempo_de_viaje = 100;
				}

				//Se calcula la velocidad del arco
				mapa.getArco(inter.id, inter_llegada.id).calcularVelocidad();

				//cambia al siguiente arco
				arco = arco.sig;
			}
		}
	}

	/**
	 * Método que escribe un archivo JSON del grafo
	 */
	@SuppressWarnings("unchecked")
	public void crearJSON()
	{	
		JSONObject vertice = new JSONObject();
		JSONArray vertices = new JSONArray();
		JSONObject arco = new JSONObject();

		int id = -1;

		Iterator<Integer> llaves = mapa.vertices.iterator();
		//Recorre todos los vertices
		while(llaves.hasNext())
		{
			//Arcos asociados a el vertice del momento
			JSONArray arcos = new JSONArray();

			id = llaves.next();

			//Vertice del momento
			Interseccion inter = mapa.getInfoVertex(id);

			//Agrega los valores del vertice al objeto JSON
			vertice.put("id", inter.id);
			vertice.put("log", inter.longitud);
			vertice.put("lat", inter.latitud);
			vertice.put("MOVEMENT_ID", inter.movement_id);

			//Nodo que recorre todos los vertices conectados a inter
			Nodo<Arco<Integer>> tempo = mapa.adj[inter.id].primero;

			//Ciclo que recorre los vertices 
			while(tempo != null)
			{
				//Añade al arreglo el id del vertice destino
				arco.put("id", tempo.value.finVertice);
				//La distancia del arco
				arco.put("costo", tempo.value.costo);
				//El tiempo del arco
				arco.put("tiempo", tempo.value.tiempo_de_viaje);
				//La velocidad del arco
				arco.put("velocidad", tempo.value.velocidad);

				arcos.add(arco);
				tempo = tempo.sig;
				arco = new JSONObject();
			}

			//Agrega los arcos asociados al vertice al objeto JSON
			vertice.put("arcos", arcos);

			vertices.add(vertice);

			vertice = new JSONObject();
		}

		//Objeto JSON con el número de vertices
		JSONObject numVertices = new JSONObject();
		numVertices.put("#Vertices", mapa.V);

		//Objeto JSON con el número de arcos
		JSONObject numArcos = new JSONObject();
		numArcos.put("#Arcos", mapa.E);

		//Objeto JSON con el número de componentes conectadas
		JSONObject numCC = new JSONObject();
		numCC.put("#CC", mapa.cc());

		//Arreglo con toda la información del grafo
		JSONArray info_grafo = new JSONArray();
		info_grafo.add(numVertices);
		info_grafo.add(numArcos);
		info_grafo.add(numCC);

		//Objeto JSON que tiene la información del grafo y la información de cada vertice
		JSONObject objeto = new JSONObject();
		objeto.put("info grafo", info_grafo);
		objeto.put("vertices", vertices);


		try (FileWriter file = new FileWriter("./data/nuevo_vertices.json")) {

			file.write(objeto.toString());
			file.flush();

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	/**
	 * Método que carga el archivo JSON del grafo
	 * @throws IOException 
	 */
	public GrafoNoDirigido<Integer, Interseccion> cargarJSON() throws IOException
	{
		GrafoNoDirigido<Integer, Interseccion> grafoJSON = null;
		long V = 0;

		JSONParser parser = new JSONParser();
		FileReader reader = null;
		try
		{
			reader = new FileReader("./data/nuevo_vertices.json");
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray info_grafo = (JSONArray)jsonObject.get("info grafo");
			JSONObject numVertices = (JSONObject) info_grafo.get(0);
			V = (long) numVertices.get("#Vertices");			
			grafoJSON = new GrafoNoDirigido<Integer, Interseccion>((int) V);

			JSONArray vertices = (JSONArray) jsonObject.get("vertices");
			for(int i = 0; i < vertices.size(); i++)
			{
				JSONObject vertice = (JSONObject) vertices.get(i);
				long id = (long) vertice.get("id");
				double log = (double) vertice.get("log");
				double lat = (double) vertice.get("lat");
				long MOVEMENT_ID = (long) vertice.get("MOVEMENT_ID");
				grafoJSON.addVertex((int)id, new Interseccion((int)id,log,lat,(int)MOVEMENT_ID));

				JSONArray arcos = (JSONArray) vertice.get("arcos");
				for(int j = 0; j < arcos.size(); j++)
				{
					JSONObject arco = (JSONObject) arcos.get(j);
					long id_arco = (long) arco.get("id");
					double costo = (double) arco.get("costo");
					if(grafoJSON.getCostArc((int) id, (int) id_arco) == -1)
					{
						grafoJSON.addEdge((int) id, (int) id_arco, costo);
						Arco<Integer> arco_temporal = grafoJSON.getArco((int) id, (int) id_arco);
						arco_temporal.tiempo_de_viaje =  (double) arco.get("tiempo");
						arco_temporal.velocidad = (double) arco.get("velocidad");
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			reader.close();
		}

		return grafoJSON;
	}

	public void cargarMapa(GrafoNoDirigido<Integer, Interseccion> grafo)
	{
		Maps mapa = new Maps(grafo);
		JFrame frame = new JFrame("Map Integration");
		//frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(mapa, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	//-----------------------------------
	// Métodos
	//-----------------------------------

	/**
	 * Dada una localización geográfica con latitud y longitud, 
	 * encontrar el Id del Vértice de la malla vial más cercano por distancia Haversine
	 * @param lat
	 * @param log
	 * @return
	 */
	public int idLocalizacionMasCercana(double lat, double log)
	{
		Iterator<Integer> iterator = mapa.vertices.iterator();
		int iterador = iterator.next();
		double minDistancia = Integer.MAX_VALUE;
		double distanciaTemp = 0;
		int id = -1;
		while(iterator.hasNext())
		{
			Interseccion inter = mapa.getInfoVertex(iterador);
			distanciaTemp = haversine(inter.latitud, inter.longitud, lat, log);
			if(distanciaTemp < minDistancia)
			{
				minDistancia = distanciaTemp;
				id = inter.id;
			}
			iterador = iterator.next();
		}
		return id;
	}

	//---------------------------------------
	// Formula Haversine 
	//---------------------------------------
	/**
	 * Método de la formula Haversine
	 * Método sacado de: https://www.geeksforgeeks.org/haversine-formula-to-find-distance-between-two-points-on-a-sphere/
	 * @param lat1 Latitud de la primera coordenada. 
	 * @param lon1 Longitud de la segunda coordenada.
	 * @param lat2 Latitud de la primera coordenada.
	 * @param lon2 Longitud de la segunda coordenada.
	 * @return
	 */
	private double haversine(double lat1, double lon1, double lat2, double lon2) 
	{ 
		// distance between latitudes and longitudes 
		double dLat = Math.toRadians(lat2 - lat1); 
		double dLon = Math.toRadians(lon2 - lon1); 

		// convert to radians 
		lat1 = Math.toRadians(lat1); 
		lat2 = Math.toRadians(lat2); 

		// apply formulae 
		double a = Math.pow(Math.sin(dLat / 2), 2) +  
				Math.pow(Math.sin(dLon / 2), 2) *  
				Math.cos(lat1) *  
				Math.cos(lat2); 
		double rad = 6371; 
		double c = 2 * Math.asin(Math.sqrt(a)); 
		return rad * c; 
	} 

	//---------------------------------------
	// Requerimientos A - Nikolas Castellanos
	//---------------------------------------
	public void caminoMasCortoTiempo(double lat1, double log1, int origen, double lat2, double log2, int destino)
	{

	}

	public void nVerticesMenorVelocidadPromedio(int n)
	{

	}

	public void MSTPrim()
	{

	}


	//-----------------------------------
	// Requerimientos B - Daniel Rincón G.
	//-----------------------------------

	/**
	 * Encontrar el camino de menor costo (menor distancia Haversine)
	 * para un viaje entre dos localizaciones geográficas de la ciudad
	 * @param lat1 Latitud del origen
	 * @param log1 Longitud del origen
	 * @param lat2 Latitud del destino
	 * @param log2 Longitud del destino
	 * @return 
	 */
	public Stack<Vertice<Integer,Interseccion>> caminoMasCortoDistancia(double lat1, double log1, double lat2, double log2)
	{
		int id_origen = idLocalizacionMasCercana(lat1, log1);
		int id_destino = idLocalizacionMasCercana(lat2,log2);

		mapa.Prim(id_origen);

		//Devolverse desde el vertice destino
		Vertice<Integer, Interseccion> target = mapa.getVertice(id_destino);
		Stack<Vertice<Integer,Interseccion>> stack = new Stack<Vertice<Integer,Interseccion>>();
		//Caso en el que no se puede llegar hasta el vertice destino
		if(target != null)
		{	
			while(target != null)
			{
				stack.push(target);
				target = target.papa;
			}
		}
		return stack;
	}

	/**
	 * A partir de las coordenadas de una localización geográfica de la ciudad de origen, 
	 * indique cuáles vértices son alcanzables para un tiempo T 
	 * @param lat Latitud del origen
	 * @param log Longitud del origen
	 * @param tiempo Tiempo máximo. tiempo > 0
	 * @return
	 */
	public Queue<Vertice<Integer,Interseccion>> verticesPorTiempo(double lat, double log, double tiempo)
	{
		int id = idLocalizacionMasCercana(lat, log);
		mapa.uncheck();
		mapa.maxCosto(id, tiempo);
		return mapa.colaMax;
	}

	/**
	 * Calcular un árbol de expansión mínima (MST) con criterio distancia
	 * aplicado al componente conectado (subgrafo) más grande de la malla vial de Bogotá.
	 * @return
	 */
	public Queue<Arco<Integer>> MSTKruskal()
	{
		int[] cc = mapa.componentesGrandes();
		int max = 0, maxi = -1;
		for(int i = 0; i < cc.length; i++)
		{
			if(cc[i] > max)
			{
				max = cc[i];
				maxi = i;
			}
		}
		Kruskal<Integer, Interseccion> kruskal = new Kruskal<Integer, Interseccion>(mapa,maxi);
		return kruskal.mst;
	}

	//-----------------------------------
	// Requerimientos C
	//-----------------------------------


	public void nuevoGrafoZonasUber()
	{
		zonas = new GrafoNoDirigido<Integer,Zona>(1160);
		Iterator<Integer> iterator = mapa.vertices.iterator();
		int iterador = iterator.next();
		Interseccion inter = mapa.getInfoVertex(iterador);
		Zona zona = null;
		//ciclo que introduce todas las zonas
		while(iterator.hasNext())
		{
			//La zona no se agregado al nuevo grafo
			if(zonas.getInfoVertex(inter.movement_id) == null)
			{
				//Se agrega una nueva zona con el movement_id nuevo y la latitud y longitud de esa interseccion
				zonas.addVertex(inter.movement_id, new Zona(inter.movement_id,inter.latitud,inter.longitud));
			}
			//Agrega a la zona todas las intersecciones que hacen parte de ella
			else
			{
				zona = zonas.getInfoVertex(inter.movement_id);
				zona.intersecciones.add(inter);
			}
			iterador = iterator.next();
			inter = mapa.getInfoVertex(iterador);
		}
		//ciclo que crea los arcos

		//las zonas con identificador: movement_id
		iterator = zonas.vertices.iterator();
		//zona en particular
		iterador = iterator.next();
		//Objeto zona en particular
		zona = zonas.getInfoVertex(iterador);

		//Nodo que contiene una interseccion de 'zona'
		Nodo<Interseccion> nodoInterseccion = null;
		//identificación de la interseccion del nodo 'nodoInterseccion'
		int id_interseccion = -1;

		//Nodo de arcos del grafo 'mapa'
		Nodo<Arco<Integer>> nodoArco; 

		//Si encontro un viaje con las zonas específicas
		boolean viajeEncontrado = false;
		
		//Ciclo que lo hace para todas las zonas
		while(iterator.hasNext())
		{
			nodoInterseccion = zona.intersecciones.primero;
			//Ciclo de todas la intersecciones por zona
			while(nodoInterseccion != null)
			{
				id_interseccion = nodoInterseccion.value.id;
				//Nodo que contiene el arco del vertice con id 'id_interseccion'
				nodoArco = mapa.adj[id_interseccion].primero;
				
				//Ciclo de todas los arcos por interseccion
				while(nodoArco != null)
				{
					int id_fin_arco = nodoArco.value.finVertice;

					//Los vertices del arco pertenecen a zonas diferentes
					if(mapa.getInfoVertex(id_fin_arco).movement_id != mapa.getInfoVertex(id_interseccion).movement_id)
					{
						//No se ha agregado el arco entre zonas
						if(zonas.getCostArc(zona.movement_id, mapa.getInfoVertex(id_fin_arco).movement_id) < 0)
						{
							//Buscar el tiempo entre zonas
							for(int i = 1; i <= 7 && !viajeEncontrado; i++)
							{
								Viaje viaje = viajes.get(mapa.getInfoVertex(id_interseccion).movement_id+"-" +mapa.getInfoVertex(id_fin_arco).movement_id + "-" + i);
								//Se encontró un viaje entre esas dos zonas
								if(viaje != null)
								{
									zonas.addEdge(zona.movement_id, mapa.getInfoVertex(id_fin_arco).movement_id, viaje.mean_travel_time);
									viajeEncontrado = true;
								}
							}
							if(!viajeEncontrado)
								zonas.addEdge(zona.movement_id, mapa.getInfoVertex(id_fin_arco).movement_id, 200);
							viajeEncontrado = false;
						}
					}
					nodoArco = nodoArco.sig;
				}
				nodoInterseccion = nodoInterseccion.sig;
			}
			
			iterador = iterator.next();
			zona = zonas.getInfoVertex(iterador);
		}
	}

	public Iterator<Arco<Integer>> Dijkstra(int zonaOrigen, int zonaDestino)
	{
		Dijkstra<Zona> dij = new Dijkstra<Zona>(zonas,zonaOrigen);
		return dij.pathTo(zonaDestino).iterator();
	}

	public Stack<Vertice<Integer, Zona>> caminoMasLargo(int zona_origen)
	{
		zonas.uncheck();
		zonas.bfs(zona_origen);
		int maxCamino = 0, idMax = 0;
		for(int i = 0; i < zonas.distTo.length;i++)
		{
			if(zonas.distTo[i] > maxCamino)
			{
				maxCamino = zonas.distTo[i];
				idMax = i;
			}
		}
		
		Vertice<Integer,Zona> target = zonas.getVertice(idMax);
		Stack<Vertice<Integer,Zona>> stack = new Stack<Vertice<Integer,Zona>>();
		//Caso en el que no se puede llegar hasta el vertice destino
		if(target != null)
		{	
			while(target != null)
			{
				stack.push(target);
				target = target.papa;
			}
		}
		return stack;
	}

}
