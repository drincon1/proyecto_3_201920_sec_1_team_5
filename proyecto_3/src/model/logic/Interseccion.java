package model.logic;

public class Interseccion 
{
	//---------------------------------------
	// Atributos
	//---------------------------------------
	/**
	 * El identificador único de la intersección
	 */
	public int id;

	/**
	 * La longitud de la intersección
	 */
	public double longitud;
	
	/**
	 * La latitud de la intersección
	 */
	public double latitud;
	
	/**
	 * La zona que le responde con respecto a las zonas Uber
	 */
	public int movement_id;
	
	//---------------------------------------
	// Constructor
	//---------------------------------------
	/**
	 * Metodo constructor del objeto Interseccion
	 * @param id Es el identificador de la interseccion. id >= 0
	 * @param longitud La longitud de la interseccion. 
	 * @param latitud La latitud de la interseccion. 
	 * @param movement_id zona uber de la interseccion. movement_id >= 0
	 */
	public Interseccion(int id, double longitud, double latitud, int movement_id) 
	{
		this.id = id;
		this.longitud = longitud;
		this.latitud = latitud;
		this.movement_id = movement_id;
	}
	
}
