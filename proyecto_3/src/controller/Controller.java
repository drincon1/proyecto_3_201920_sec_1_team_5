package controller;

import java.io.IOException;
import java.util.Iterator;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.Vertice;
import model.data_structures.Zona;
import model.logic.Interseccion;
import model.logic.MVCModelo;
import view.MVCView;

public class Controller 
{
	//-----------------------------------
	// Atributos
	//-----------------------------------
	/**
	 * Conexión con el view
	 */
	public MVCView view;

	/**
	 * Conexión con el modelo
	 */
	public MVCModelo modelo;

	/**
	 * Inicio del cronómetro
	 */
	private long startTime;

	/**
	 * Fin del cronómetro
	 */
	private long endTime;

	/**
	 * Resta: startTime - endTime
	 */
	private long duration;
	//-----------------------------------
	// Constructor
	//-----------------------------------
	/**
	 * Constructor de la clase Controller
	 */
	public Controller() 
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	//-----------------------------------
	// Métodos
	//-----------------------------------	

	/**
	 * Método que corre el programa
	 * @throws IOException 
	 */
	public void run() throws IOException
	{
		startTime = System.currentTimeMillis();

		//llama al método del modelo que carga la información necesaria
		modelo.cargar();

		endTime = System.currentTimeMillis();
		duration = endTime - startTime;

		//imprime la cantidad de tiempo que se demoró cargar()
		view.printTiempo(duration, "Cargar la información se demoró: ");
		//Imprimer una separación
		view.printSim();

		//imprime la información ya cargada
		view.printCargar(modelo.viajes,modelo.mapa);

		view.printSim();

		startTime = System.currentTimeMillis();
		//llama al método del modelo para cambiar los costos de los arcos
		modelo.cambiarCostos();
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;

		//imprime la cantidad de tiempo que se demoró en cambiar los costos
		view.printTiempo(duration, "Cambiar los costos de los arcos se demoró: ");

		view.printSim();

		startTime = System.currentTimeMillis();
		//crea el nuevo archivo JSON
		modelo.crearJSON();
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;

		//imprime la cantidad de tiempo que se demoró en crear el archivo JSON
		view.printTiempo(duration, "Crear el nuevo archivo JSON se demoró: ");
		
		view.printSim();
		
		startTime = System.currentTimeMillis();
		//lee el nuevo archivo JSON
		GrafoNoDirigido<Integer, Interseccion> grafo_temporal = modelo.cargarJSON();
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;

		//imprime la cantidad de tiempo que se demoró en leer el nuevo archivo JSON
		view.printTiempo(duration, "Leer el nuevo archivo JSON se demoró: ");
		
		view.printNuevoJSON(grafo_temporal);
		
		view.printSim();

		//modelo.cargarMapa(modelo.mapa);
		//--------------------------------------------------------------------------
		// Interacción con el usuario
		//--------------------------------------------------------------------------
		view.printSim();
		view.printMenu();
		view.printMensaje("¿Cuál opción quiere?");
		int opcion = Integer.parseInt(view.scannerString());
		view.printSim();
		double lat1 = 0, lat2 = 0;
		double log1 = 0, log2 = 0;
		double tiempo = 0;
		int zona_origen, zona_destino;
		while(opcion != -1)
		{
			switch(opcion)
			{
			case 7: 
				view.printMensaje("Latitud origen: ");
				lat1 = Double.parseDouble(view.scannerString());
				view.printMensaje("Longitud origen: ");
				log1 = Double.parseDouble(view.scannerString());
				view.printMensaje("Latitud destino: ");
				lat2 = Double.parseDouble(view.scannerString());
				view.printMensaje("Longitud destino: ");
				log1 = Double.parseDouble(view.scannerString());
				view.printCaminoMasCortoDistancia(modelo.caminoMasCortoDistancia(lat1, log1, lat2, log2), modelo.mapa);
				break;
			case 8:
				view.printMensaje("Latitud origen: ");
				lat1 = Double.parseDouble(view.scannerString());
				view.printMensaje("Longitud origen: ");
				log1 = Double.parseDouble(view.scannerString());
				view.printMensaje("Tiempo: ");
				tiempo = Double.parseDouble(view.scannerString());
				view.printVerticesPorTiempo(modelo.verticesPorTiempo(lat1, log1, tiempo));
				break;
			case 9:
				startTime = System.currentTimeMillis();
				//Método Kruskal
				Queue<Arco<Integer>> mst = modelo.MSTKruskal();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printTiempo(duration/1000, "MSTKruskal en la componente conectada con más vertices se demoró: ");
				view.printMSTKruskal(mst, modelo.mapa);
				break;
			case 10:
				modelo.nuevoGrafoZonasUber();
				view.printNuevoGrafoZonasUber(modelo.zonas);
				break;
			case 11:
				view.printMensaje("Zona origen: ");
				zona_origen = Integer.parseInt(view.scannerString());
				view.printMensaje("Zona destino: ");
				zona_destino = Integer.parseInt(view.scannerString());
				startTime = System.currentTimeMillis();
				//Método Kruskal
				Iterator<Arco<Integer>> iterator = modelo.Dijkstra(zona_origen, zona_destino);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printTiempo(duration/1000, "Hacer el camino de costo mínimo se demoró: ");
				view.printDijkstra(iterator,modelo.viajes,zona_origen,zona_destino);
				break;
			case 12:
				view.printMensaje("Zona origen: ");
				zona_origen = Integer.parseInt(view.scannerString());
				startTime = System.currentTimeMillis();
				//Método caminoMasLargo
				Stack<Vertice<Integer, Zona>> stack = modelo.caminoMasLargo(zona_origen);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printTiempo(duration/1000, "Hacer el camino de costo máximo (arcos) se demoró: ");
				view.printCaminoMasLargo(stack,modelo.zonas);
				break;
			}
			view.printSim();
			view.printMenu();
			view.printMensaje("¿Cuál opción quiere?");
			opcion = Integer.parseInt(view.scannerString());
			view.printSim();
		}
	}






}
