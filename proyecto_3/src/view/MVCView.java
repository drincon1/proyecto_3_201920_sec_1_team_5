package view;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.HashTable;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.Vertice;
import model.data_structures.Zona;
import model.logic.Interseccion;
import model.logic.MVCModelo;
import model.logic.Viaje;
import ordenamientos.QuickSort;

public class MVCView 
{
	//-----------------------------------
	// Atributos
	//-----------------------------------
	ViewMapa mapaD;
	
	public Scanner scanner;
	//-----------------------------------
	// Constructor
	//-----------------------------------
	public MVCView()
	{
		scanner = new Scanner(System.in);
	}

	//-----------------------------------
	// Métodos asistentes
	//-----------------------------------
	/**
	 * Método que imprime un mensaje en específico
	 * @param mensaje Mensaje que se va a imprimir. mensaje != null
	 */
	public void printMensaje(String mensaje)
	{
		System.out.println(mensaje);
	}

	public void printTiempo(long duration, String mensaje)
	{
		printMensaje(mensaje + "" + (duration/1000.0) + " seg");
	}

	public void printSim()
	{
		printMensaje("---------------------------");
	}

	
	public String scannerString()
	{
		return scanner.next();
	}
	public void printMenu() 
	{
		printMensaje("7) Encontrar el camino de menor costo para un viaje entre dos localizaciones geográficas de la ciudad");
		printMensaje("8) A partir de las coordenadas de una localización geográfica de la ciudad (lat, lon) de origen, cuáles vértices son alcanzables para un tiempo T ");
		printMensaje("9) Calcular un árbol de expansión mínima con criterio distancia, aplicado al componente conectado más grande de la malla vial de Bogotá.");
		printMensaje("10) Construir un nuevo grafo simplificado No dirigido de las zonas Uber");
		printMensaje("11) Calcular el camino de costo mínimo basado en el tiempo promedio entre una zona de origen y una zona de destino sobre el grafo de zonas.");
		printMensaje("12) A partir de una zona origen, calcular los caminos de menor longitud");
	}

	//-----------------------------------
	// Métodos 
	//-----------------------------------
	/**
	 * Imprime la información del método cargar
	 * @param viajes Tabla de hash con los viajes ya cargados. viajes != null
	 */
	public void printCargar(HashTable<String, Viaje> viajes, GrafoNoDirigido<Integer, Interseccion> grafo)
	{
		//Imprime la cantidad de viajes en la tabla
		printMensaje("Número de viajes: " + viajes.size());

		//Imprime el número de zonas
		printMensaje("Número de zonas: " + grafo.V);

		//Imprime el número de arcos
		printMensaje("Número de conexiones " + grafo.E);

		//Imprime el número de componentes conectadas
		printMensaje("Número de componentes conectadas: " + grafo.cc());

		int[] arreglo2 = grafo.componentesGrandes();
		Integer[] arreglo = new Integer[arreglo2.length];
		for(int i = 0; i < arreglo2.length; i++)
			arreglo[i] = arreglo2[i];
		QuickSort<Integer> quickSort = new QuickSort<Integer>(arreglo);
		printMensaje("Número de vertices en las 5 componentes más grandes: ");
		for(int i = 0; i < 5; i++)
			printMensaje((i+1) + ") " + arreglo[i]);
	}

	public void printNuevoJSON(GrafoNoDirigido<Integer, Interseccion> grafo)
	{
		//Imprime el número de zonas
		printMensaje("Número de zonas: " + grafo.V);

		//Imprime el número de arcos
		printMensaje("Número de conexiones " + grafo.E);
	}

	public void printMapa(ViewMapa mapa)
	{
		JFrame frame = new JFrame("Map Integration");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(mapa, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	//-----------------------------------
	// Print Requerimientos B - Daniel Rincón G.
	//-----------------------------------


	public void printCaminoMasCortoDistancia(Stack<Vertice<Integer,Interseccion>> camino, GrafoNoDirigido<Integer, Interseccion> mapa)
	{
		if(!camino.isEmpty())
		{
			Stack<Vertice<Integer,Interseccion>> caminoMapa = camino;
			printMensaje("Número de vertices: " + camino.size());
			Vertice<Integer, Interseccion> anterior = camino.pop();
			Vertice<Integer, Interseccion> actual = camino.pop();
			Arco<Integer> arco = null;
			if(anterior != null && actual != null)
				arco = mapa.getArco(anterior.identificacion, actual.identificacion);
			double tiempo_total = 0, distancia_total = 0;
			while(actual != null)
			{
				printMensaje("Desde: " + arco.iniVertice + " hasta: " + arco.finVertice + " costó: " + arco.costo);
				tiempo_total += arco.tiempo_de_viaje;
				distancia_total += arco.costo;

				anterior = actual;
				actual = camino.pop();
				if(actual != null)
					arco = mapa.getArco(anterior.identificacion, actual.identificacion);
			}
			printMensaje("La distancia total es: " + distancia_total + " km");
			printMensaje("El tiempo total es: "+ tiempo_total + " s");

			mapaD = new ViewMapa(caminoMapa,7);
			printMapa(mapaD);

		}
		else
			printMensaje("No se encontró un camino entre las localizaciones");


	}

	public void printVerticesPorTiempo(Queue<Vertice<Integer,Interseccion>> vertices)
	{
		if(!vertices.isEmpty())
		{
			Queue<Vertice<Integer,Interseccion>> verticesMapa = vertices; 
			printMensaje("Desde el vertice con identificador " + vertices.dequeue().identificacion + " se puede llegar hasta: ");
			Vertice<Integer, Interseccion> vertice = vertices.dequeue();
			while(vertice != null)
			{
				printMensaje("ID: " + vertice.identificacion + " lat: " + vertice.valor.latitud + " log: " + vertice.valor.longitud);
				vertice = vertices.dequeue();
			}
			mapaD = new ViewMapa(verticesMapa,8);
			printMapa(mapaD);

		}
		else
			printMensaje("No se puede llegar a locaciones con ese tiempo");
	}

	public void printMSTKruskal(Queue<Arco<Integer>> mst, GrafoNoDirigido<Integer, Interseccion> mapa)
	{
		if(!mst.isEmpty())
		{
			Queue<Arco<Integer>> mstMapa = mst;
			printMensaje("Número de vertices: " + mst.size());
			Arco<Integer> arco = mst.dequeue();
			double costo_total = 0;
			while(arco != null)
			{
				printMensaje("Desde: " + arco.iniVertice + " hasta: " + arco.finVertice);
				costo_total += arco.costo;
				arco = mst.dequeue();
			}
			printMensaje("Costo total: " + costo_total);
			mapaD = new ViewMapa(9, mstMapa,mapa);
			printMapa(mapaD);
		}
		else
			printMensaje("Error calculando el Kruskal");
	}

	//-----------------------------------
	// Print Requerimientos C 
	//-----------------------------------	

	public void printNuevoGrafoZonasUber(GrafoNoDirigido<Integer,Zona> grafo)
	{
		printMensaje("Número de vertices: " + grafo.V);
		printMensaje("Número de arcos: " + grafo.E);
	}

	public void printDijkstra(Iterator<Arco<Integer>> iterator, HashTable<String, Viaje> viajes, int zona_origen, int zona_destino)
	{
		if(iterator.hasNext())
		{
			Arco<Integer> arco = iterator.next();
			int i = 1;
			double costo_total = 0;
			while(iterator.hasNext())
			{
				printMensaje(i + ") " + arco.iniVertice + "-->" + arco.finVertice);
				i++;
				costo_total += arco.costo;
				arco = iterator.next();
			}
			printMensaje("Costo total: " + costo_total);
			
			for(int j = 1; j <= 7; j++)
			{
				Viaje viaje = viajes.get(zona_origen+"-"+zona_destino+"-"+i);
				if(viaje != null)
					printMensaje("En el día " + i + " de la semana, el tiempo promedio fue: " + viaje.mean_travel_time);
			}
		}
		else
			printMensaje("Error calculando Dijkstra");
	}

	public void printCaminoMasLargo(Stack<Vertice<Integer,Zona>> camino, GrafoNoDirigido<Integer, Zona> zonas)
	{
		if(!camino.isEmpty())
		{
			printMensaje("Número de vertices: " + camino.size());
			Vertice<Integer,Zona> anterior = camino.pop();
			Vertice<Integer,Zona> actual = camino.pop();
			Arco<Integer> arco = zonas.getArco(anterior.identificacion, actual.identificacion);
			int num_arcos = 1;
			while(actual != null)
			{
				
				printMensaje(arco.iniVertice+"-->"+arco.finVertice);
				anterior = actual;
				actual = camino.pop();
				if(actual != null)
				{
					arco = zonas.getArco(anterior.identificacion, actual.identificacion);
					num_arcos++;
				}
			}
			printMensaje("Número de arcos: " + num_arcos);
		}
		else
			printMensaje("Error calculando camino mas largo");
	}



}
