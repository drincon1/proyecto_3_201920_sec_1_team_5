package view;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.Vertice;
import model.logic.Interseccion;

public class ViewMapa extends MapView
{
	Stack<Vertice<Integer,Interseccion>> caminoMapa;
	Queue<Vertice<Integer,Interseccion>> vertices;
	Queue<Arco<Integer>> mst;
	GrafoNoDirigido<Integer, Interseccion> mapa;
	private Map map;

	//-------------------------------------------
	// Constructor mapa punto 7
	//-------------------------------------------
	public ViewMapa(Stack<Vertice<Integer,Interseccion>> caminoMapa, int i)
	{
		this.caminoMapa = caminoMapa;

		setOnMapReadyHandler(new MapReadyHandler() 
		{
			public void onMapReady(MapStatus status) 
			{
				if (status == MapStatus.MAP_STATUS_OK) 
				{
					map = getMap();
					initMap(map,i);
				}
			}
		});
	}
	//-------------------------------------------
	// Constructor mapa punto 8
	//-------------------------------------------
	public ViewMapa(Queue<Vertice<Integer,Interseccion>> vertices, int i)
	{
		this.vertices = vertices;

		setOnMapReadyHandler(new MapReadyHandler() 
		{
			public void onMapReady(MapStatus status) 
			{
				if (status == MapStatus.MAP_STATUS_OK) 
				{
					map = getMap();
					initMap(map,i);
				}
			}
		});
	}
	//-------------------------------------------
	// Constructor mapa punto 9
	//-------------------------------------------
	public ViewMapa(int i,Queue<Arco<Integer>> mst, GrafoNoDirigido<Integer, Interseccion> mapa)
	{
		this.mst = mst;
		this.mapa = mapa;
		setOnMapReadyHandler(new MapReadyHandler() 
		{
			public void onMapReady(MapStatus status) 
			{
				if (status == MapStatus.MAP_STATUS_OK) 
				{
					map = getMap();
					initMap(map,i);
				}
			}
		});
	}




	//-------------------------------------------
	// Mapa
	//-------------------------------------------
	public void initMap(Map map, int i)
	{
		MapOptions options = new MapOptions();
		MapTypeControlOptions controlOptions = new MapTypeControlOptions();
		controlOptions.setPosition(ControlPosition.TOP_RIGHT);

		options.setMapTypeControlOptions(controlOptions);

		map.setOptions(options);
		map.setCenter(new LatLng(4.597714, -74.094723));
		map.setZoom(12.0);

		CircleOptions co = new CircleOptions();
		co.setFillOpacity(0.35);
		co.setStrokeWeight(1);
		co.setStrokeOpacity(0.2);
		co.setStrokeColor("00FF00");

		PolylineOptions polyoptions = new PolylineOptions();
		polyoptions.setGeodesic(false);
		polyoptions.setStrokeColor("#FF0000");
		polyoptions.setStrokeOpacity(1.0);
		polyoptions.setStrokeWeight(2.0);

		//punto 7
		if(i == 7)
		{
			Vertice<Integer, Interseccion> anterior = caminoMapa.pop();
			Vertice<Integer, Interseccion> actual = caminoMapa.pop();	
			Circle circle = new Circle(map);
			circle.setCenter(new LatLng(anterior.valor.latitud, anterior.valor.longitud));
			circle.setOptions(co);
			circle.setRadius(10);
			while(actual != null)
			{
				circle = new Circle(map);
				circle.setCenter(new LatLng(actual.valor.latitud, actual.valor.longitud));
				circle.setOptions(co);
				circle.setRadius(10);

				LatLng[] path = {new LatLng(anterior.valor.latitud, anterior.valor.longitud),new LatLng(actual.valor.latitud, actual.valor.longitud)};
				Polyline polyline = new Polyline(map);
				polyline.setPath(path);
				polyline.setOptions(polyoptions);

				anterior = actual;
				actual = caminoMapa.pop();
			}
		}
		//punto 8
		else if(i == 8)
		{
			CircleOptions co2 = new CircleOptions();
			co2.setFillOpacity(0.35);
			co2.setStrokeWeight(1);
			co2.setStrokeOpacity(0.2);
			co2.setStrokeColor("0000FF");

			Vertice<Integer,Interseccion> vertice = vertices.dequeue();
			Circle circle = new Circle(map);
			circle.setCenter(new LatLng(vertice.valor.latitud, vertice.valor.longitud));
			circle.setOptions(co);
			circle.setRadius(10);

			vertice = vertices.dequeue();
			while(vertice != null)
			{
				circle = new Circle(map);
				circle.setCenter(new LatLng(vertice.valor.latitud, vertice.valor.longitud));
				circle.setOptions(co2);
				circle.setRadius(10);
				vertice = vertices.dequeue();
			}



		}
		//punto 9
		else if(i == 9)
		{
			Arco<Integer> arco = mst.dequeue();
			while(arco != null)
			{
				Vertice<Integer, Interseccion> v1 = mapa.getVertice(arco.iniVertice);
				LatLng latlngv1 = new LatLng(v1.valor.latitud, v1.valor.longitud);
				Vertice<Integer, Interseccion> v2 = mapa.getVertice(arco.finVertice);
				LatLng latlngv2 = new LatLng(v2.valor.latitud, v2.valor.longitud);
				
				Circle circle = new Circle(map);
				circle.setCenter(latlngv1);
				circle.setOptions(co);
				circle.setRadius(10);
				
				circle = new Circle(map);
				circle.setCenter(latlngv2);
				circle.setOptions(co);
				circle.setRadius(10);
				
				LatLng[] path = {latlngv1,latlngv2};
				Polyline polyline = new Polyline(map);
				polyline.setPath(path);
				polyline.setOptions(polyoptions);
				
				
				arco = mst.dequeue();
			}
		}
		else if(i == 10)
		{

		}
		else if(i == 11)
		{

		}
		else if(i == 12)
		{

		}
	}

	public Map getMap()
	{
		return map;
	}
}
